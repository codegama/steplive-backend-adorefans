<?php

use Carbon\Carbon;

// Helper, Setting, Log;

use App\User, App\PageCounter, App\Settings;

use App\SubscriptionPayment;

use App\MeetingUser;

/**
 * @method tr()
 *
 * Description: used to convert the string to language based string
 *
 * @created Vidhya R
 *
 * @updated
 *
 * @param string $key
 *
 * @return string value
 */
function tr($key , $additional_key = "" , $lang_path = "messages.") {

    // if(Auth::guard('admin')->check()) {

    //     $locale = config('app.locale');

    // } else {

        if (!\Session::has('locale')) {

            $locale = \Session::put('locale', config('app.locale'));

        }else {

            $locale = \Session::get('locale');

        }
    // }
    return \Lang::choice('messages.'.$key, 0, Array('other_key' => $additional_key), $locale);

}

function api_success($key , $other_key = "" , $lang_path = "messages.") {

    if (!\Session::has('locale')) {

        $locale = \Session::put('locale', config('app.locale'));

    } else {

        $locale = \Session::get('locale');

    }
    return \Lang::choice('api-success.'.$key, 0, Array('other_key' => $other_key), $locale);

}

function api_error($key , $other_key = "" , $lang_path = "messages.") {

    if (!\Session::has('locale')) {

        $locale = \Session::put('locale', config('app.locale'));

    } else {

        $locale = \Session::get('locale');

    }
    return \Lang::choice('api-error.'.$key, 0, Array('other_key' => $other_key), $locale);

}

/**
 * @method envfile()
 *
 * Description: get the configuration value from .env file 
 *
 * @created Vidhya R
 *
 * @updated
 *
 * @param string $key
 *
 * @return string value
 */

function envfile($key) {

    $data = getEnvValues();

    if($data) {
        return $data[$key];
    }

    return "";

}

function getEnvValues() {

    $data =  [];

    $path = base_path('.env');

    if(file_exists($path)) {

        $values = file_get_contents($path);

        $values = explode("\n", $values);

        foreach ($values as $key => $value) {

            $var = explode('=',$value);

            if(count($var) == 2 ) {
                if($var[0] != "")
                    $data[$var[0]] = $var[1] ? $var[1] : null;
            } else if(count($var) > 2 ) {
                $keyvalue = "";
                foreach ($var as $i => $imp) {
                    if ($i != 0) {
                        $keyvalue = ($keyvalue) ? $keyvalue.'='.$imp : $imp;
                    }
                }
                $data[$var[0]] = $var[1] ? $keyvalue : null;
            }else {
                if($var[0] != "")
                    $data[$var[0]] = null;
            }
        }

        array_filter($data);
    
    }

    return $data;

}

/**
 * @method register_mobile()
 *
 * Description: Update the user register device details 
 *
 * @created Vidhya R
 *
 * @updated
 *
 * @param string $device_type
 *
 * @return - 
 */

function register_mobile($device_type) {

    // if($reg = MobileRegister::where('type' , $device_type)->first()) {

    //     $reg->count = $reg->count + 1;

    //     $reg->save();
    // }
    
}

/**
 * @method subtract_count()
 *
 * @uses While Delete user, subtract the count from mobile register table based on the device type
 *
 * @created vithya R
 *
 * @updated vithya R
 *
 * @param string $device_ype : Device Type (Andriod,web or IOS)
 * 
 * @return boolean
 */

function subtract_count($device_type) {

    if($reg = MobileRegister::where('type' , $device_type)->first()) {

        $reg->count = $reg->count - 1;
        
        $reg->save();
    }

}

/**
 * @method get_register_count()
 *
 * Description: Get no of register counts based on the devices (web, android and iOS)
 *
 * @created Vidhya R
 *
 * @updated
 *
 * @param - 
 *
 * @return array value
 */

function get_register_count() {

    $ios_count = MobileRegister::where('type' , 'ios')->get()->count();

    $android_count = MobileRegister::where('type' , 'android')->get()->count();

    $web_count = MobileRegister::where('type' , 'web')->get()->count();

    $total = $ios_count + $android_count + $web_count;

    return array('total' => $total , 'ios' => $ios_count , 'android' => $android_count , 'web' => $web_count);

}

/**
 * @method: last_x_days_page_view()
 *
 * @uses: to get last x days page visitors analytics
 *
 * @created Anjana H
 *
 * @updated Anjana H
 *
 * @param - 
 *
 * @return array value
 */
function last_x_days_page_view($days){

    // $views = PageCounter::orderBy('created_at','asc')->where('created_at', '>', Carbon::now()->subDays($days))->where('page','home');
 
    // $arr = array();
 
    // $arr['count'] = $views->count();

    // $arr['get'] = $views->get();

    //   return $arr;
}

/**
 * @method last_x_days_revenue()
 *
 * @uses to get revenue analytics 
 *
 * @created Anjana H
 * 
 * @updated Anjana H
 * 
 * @param  integer $days
 * 
 * @return array of revenue totals
 */
function last_x_days_revenue($days) {
            
    $data = new \stdClass;

    $data->currency = $currency = Setting::get('currency', '$');

    // Last 10 days revenues

    $last_x_days_revenues = [];

    $start  = new \DateTime('-7 day', new \DateTimeZone('UTC'));
    
    $period = new \DatePeriod($start, new \DateInterval('P1D'), $days);
   
    $dates = $last_x_days_revenues = [];

    foreach ($period as $date) {

        $current_date = $date->format('Y-m-d');

        $current_day = $date->format('l');

        $last_x_days_data = new \stdClass;

        $last_x_days_data->date = $current_date;

        $last_x_days_data->day = $current_day;
      
        $last_x_days_total_subscriptions =  SubscriptionPayment::whereDate('created_at', '=', $current_date)->count() ?: 0;
      
        $last_x_days_data->total_subscriptions = $last_x_days_total_subscriptions ?: 0.00;

        $last_x_days_total_users = User::whereDate('created_at', '=', $current_date)->count();

        $last_x_days_data->total_users =  $last_x_days_total_users ?: 0;

        $last_x_days_total_earnings = SubscriptionPayment::whereDate('paid_date', '=', $current_date)->where('status', PAID)->sum('amount');
        
        $last_x_days_data->total_earnings = $last_x_days_total_earnings ?: 0.00;

        array_push($last_x_days_revenues, $last_x_days_data);

    }
    
    $data->last_x_days_revenues = $last_x_days_revenues;
    
    return $data;   

}
function counter($page = 'home') {

    // $count_home = PageCounter::wherePage($page)->where('created_at', '>=', new DateTime('today'));

    //     if($count_home->count() > 0) {
    //         $update_count = $count_home->first();
    //         $update_count->unique_id = uniqid();
    //         $update_count->count = $update_count->count + 1;
    //         $update_count->save();
    //     } else {
    //         $create_count = new PageCounter;
    //         $create_count->page = $page;
    //         $create_count->unique_id = uniqid();
    //         $create_count->count = 1;
    //         $create_count->save();
    //     }

}

//this function convert string to UTC time zone

function convertTimeToUTCzone($date, $userTimezone, $format = 'Y-m-d H:i:s') {

    $new_date = new DateTime($date, new DateTimeZone($userTimezone));

    $new_date->setTimeZone(new DateTimeZone('UTC'));

    return $new_date->format( $format);
}

//this function converts string from UTC time zone to current user timezone

function convertTimeToUSERzone($str, $userTimezone, $format = 'Y-m-d H:i:s') {

    if(empty($str)){
        return '';
    }
    
    try {
        
        $new_str = new DateTime($str, new DateTimeZone('UTC') );
        
        $new_str->setTimeZone(new DateTimeZone( $userTimezone ));
    }
    catch(\Exception $e) {
        // Do Nothing
    }
    
    return $new_str->format( $format);
}

function number_format_short( $n, $precision = 1 ) {

    if ($n < 900) {
        // 0 - 900
        $n_format = number_format($n, $precision);
        $suffix = '';
    } else if ($n < 900000) {
        // 0.9k-850k
        $n_format = number_format($n / 1000, $precision);
        $suffix = 'K';
    } else if ($n < 900000000) {
        // 0.9m-850m
        $n_format = number_format($n / 1000000, $precision);
        $suffix = 'M';
    } else if ($n < 900000000000) {
        // 0.9b-850b
        $n_format = number_format($n / 1000000000, $precision);
        $suffix = 'B';
    } else {
        // 0.9t+
        $n_format = number_format($n / 1000000000000, $precision);
        $suffix = 'T';
    }
  // Remove unecessary zeroes after decimal. "1.0" -> "1"; "1.00" -> "1"
  // Intentionally does not affect partials, eg "1.50" -> "1.50"
    if ( $precision > 0 ) {
        $dotzero = '.' . str_repeat( '0', $precision );
        $n_format = str_replace( $dotzero, '', $n_format );
    }
    return $n_format . $suffix;

}

function common_date($date, $timezone = "" , $format = "d M Y h:i A") {
    
    if($timezone) {

        $date = convertTimeToUSERzone($date , $timezone , $format);

    }   
   
    return date($format , strtotime($date));
}

function common_server_date($date, $timezone = "" , $format = "Y-m-d H:i:s") {
    
    if($timezone) {

        $date = convertTimeToUTCzone($date, $timezone , $format);

    }   
   
    return date($format , strtotime($date));
}


/**
 * function delete_value_prefix()
 * 
 * @uses used for concat string, while deleting the records from the table
 *
 * @created vidhya R
 *
 * @updated vidhya R
 *
 * @param $prefix - from settings table (Setting::get('prefix_user_delete'))
 *
 * @param $primary_id - Primary ID of the delete record
 *
 * @param $is_email 
 *
 * @return concat string based on the input values
 */

function delete_value_prefix($prefix , $primary_id , $is_email = 0) {

    if($is_email) {

        $site_name = str_replace(' ', '_', Setting::get('site_name'));

        return $prefix.$primary_id."@".$site_name.".com";
        
    } else {
        return $prefix.$primary_id;

    }

}

/**
 * function routefreestring()
 * 
 * @uses used for remove the route parameters from the string
 *
 * @created vidhya R
 *
 * @updated vidhya R
 *
 * @param string $string
 *
 * @return Route parameters free string
 */

function routefreestring($string) {

    $string = preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $string));
    
    $search = [' ', '&', '%', "?",'=','{','}','$'];

    $replace = ['-', '-', '-' , '-', '-', '-' , '-','-'];

    $string = str_replace($search, $replace, $string);

    return $string;
    
}

/**
 * @method selected()
 *
 * @uses set selected item 
 *
 * @created Anjana H
 *
 * @updated Anjana H
 *
 * @param $array, $id, $check_key_name
 *
 * @return response of array 
 */
function selected($array, $id, $check_key_name) {
    
    $is_key_array = is_array($id);
    
    foreach ($array as $key => $array_details) {

        $array_details->is_selected = ($array_details->$check_key_name == $id) ? YES : NO;
    }  

    return $array;
}


function nFormatter($num, $currency = "") {

    $currency = Setting::get('currency', "$");

    if($num>1000) {

        $x = round($num);

        $x_number_format = number_format($x);

        $x_array = explode(',', $x_number_format);

        $x_parts = ['k', 'm', 'b', 't'];

        $x_count_parts = count($x_array) - 1;

        $x_display = $x;

        $x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');

        $x_display .= $x_parts[$x_count_parts - 1];

        return $currency." ".$x_display;

    }

    return $currency." ".$num;

}

/**
 * @method formatted_plan()
 *
 * @uses used to format the number
 *
 * @created Bhawya
 *
 * @updated Akshata
 *
 * @param integer $num
 * 
 * @param string $currency
 *
 * @return string $formatted_plan
 */

function formatted_plan($plan = 0, $type = "month") {

    switch ($type) {

        case PLAN_TYPE_WEEK:

            $text = $plan <= 1 ? tr('week') : tr('weeks');

            break;

        case PLAN_TYPE_DAY:

            $text = $plan <= 1 ? tr('day') : tr('days');

            break;

        case PLAN_TYPE_YEAR:

            $text = $plan <= 1 ? tr('year') : tr('years');

            break;
        
        default:
        
            $text = $plan <= 1 ? tr('month') : tr('months');
            
            break;
    }
    
    return $plan." ".$text;
}

/**
 * @method formatted_amount()
 *
 * @uses used to format the number
 *
 * @created vidhya R
 *
 * @updated vidhya R
 *
 * @param integer $num
 * 
 * @param string $currency
 *
 * @return string $formatted_amount
 */

function formatted_amount($amount = 0.00, $currency = "") {
   
    $currency = $currency ?: Setting::get('currency', '$');

    $amount = number_format((float)$amount, 2, '.', '');

    $formatted_amount = $currency."".$amount ?: "0.00";

    return $formatted_amount;
}

function readFileLength($file)  {

    $variableLength = 0;
    if (($handle = fopen($file, "r")) !== FALSE) {
         $row = 1;
         while (($data = fgetcsv($handle, 1000, "\n")) !== FALSE) {
            $num = count($data);
            $row++;
            for ($c=0; $c < $num; $c++) {
                $exp = explode("=>", $data[$c]);
                if (count($exp) == 2) {
                    $variableLength += 1; 
                }
            }
        }
        fclose($handle);
    }

    return $variableLength;
}

function no_of_users_formatted($no_of_users = 1) {

    $text = $no_of_users <= 1 ? tr('user') : tr('users');

    $formatted = $no_of_users." ".$text;

    return $formatted;

}

function no_of_minutes_formatted($no_of_minutes) {

    $text = $no_of_minutes <= 1 ? tr('minute') : tr('minutes');

    $formatted = $no_of_minutes." ".$text;

    return $formatted;

}

function meeting_status($status) {

    $status_list = [
        MEETING_NONE => tr('MEETING_NONE'), 
        MEETING_SCHEDULED => tr('MEETING_SCHEDULED'), 
        MEETING_STARTED => tr('MEETING_STARTED'), 
        MEETING_ENDED => tr('MEETING_ENDED'),
        MEETING_CANCELLED => tr('MEETING_CANCELLED'),
    ];

    return isset($status_list[$status]) ? $status_list[$status] : tr('paid');


}

function getExpiryDate($plan,$plan_type) {
   
    $expiry_date = date('Y-m-d h:i:s',strtotime("+{$plan}{$plan_type}"));

    return $expiry_date;
}

/**
 * @method last_x_days_revenue()
 *
 * @uses to get revenue analytics 
 *
 * @created Akshata
 * 
 * @updated 
 * 
 * @param  integer $days
 * 
 * @return array of revenue totals
 */
function last_x_days_upcoming_bookings($days, $user_id) {
            
    $data = new \stdClass;

    $data->currency = $currency = Setting::get('currency', '$');

    // Last 10 days revenues

    $last_x_days_bookings = [];

    $start  = new \DateTime('-7 day', new \DateTimeZone('UTC'));
    
    $period = new \DatePeriod($start, new \DateInterval('P1D'), $days);
   
    $meeting_data = $last_x_days_bookings = [];

    foreach ($period as $date) {

        $current_date = $date->format('Y-m-d');

        $last_x_days_data = new \stdClass;

        $last_x_days_data->date = $current_date;
      
        $meeting_data = \App\Meeting::whereDate('meetings.created_at', '>=', $current_date)
                        ->where('meetings.user_id', $user_id)
                        ->where('meetings.meeting_type', MEETING_TYPE_UPCOMING)
                        ->count();

        $last_x_days_data->total_bookings = $meeting_data ?: 0;

        array_push($last_x_days_bookings, $last_x_days_data);

    }
    
    $data->last_x_days_bookings = $last_x_days_bookings;
    
    return $data;   

}

function static_page_footers($section_type = 0, $is_list = NO) {

    $lists = [
                STATIC_PAGE_SECTION_1 => tr('STATIC_PAGE_SECTION_1')."(".Setting::get('site_name').")",
                STATIC_PAGE_SECTION_2 => tr('STATIC_PAGE_SECTION_2')."(Features)",
                STATIC_PAGE_SECTION_3 => tr('STATIC_PAGE_SECTION_3')."(Learn & Connect)"
            ];

    if($is_list == YES) {
        return $lists;
    }

    return isset($lists[$section_type]) ? $lists[$section_type] : "Common";

}

function get_bbb_base_url() {

    return Setting::get('BBB_ENV') == 'production' ? Setting::get('BBB_SERVER_BASE_URL_PRODUCTION') : Setting::get('BBB_SERVER_BASE_URL_DEBUG');

}

function get_bbb_secret() {

    return Setting::get('BBB_ENV') == 'production' ? Setting::get('BBB_SECRET_PRODUCTION') : Setting::get('BBB_SECRET_DEBUG');

}

function get_meeting_status_error($status) {

    $status_list = [
        MEETING_NONE => api_error(141), 
        MEETING_SCHEDULED => api_error(141), 
        MEETING_ENDED => api_error(143),
        MEETING_CANCELLED => api_error(144),
    ];

    return isset($status_list[$status]) ? $status_list[$status] : api_error(141);

}

function get_overall_record_url($bbb_record_id) {

    // sample url - https://media.appswamy.com/download/presentation/208f1412c4be94443324808ea1f8efed08170423-1604588349145/208f1412c4be94443324808ea1f8efed08170423-1604588349145.mp4

    $base_url = Setting::get('BBB_ENV') == 'production' ? Setting::get('BBB_RECORD_PRESENTATION_URL_PRO') : Setting::get('BBB_RECORD_PRESENTATION_URL_DEBUG');

    $url = $base_url.$bbb_record_id.'/'.$bbb_record_id.'.mp4';

    return $url ? $url : "";

    // return remote_file_exists($url) ? $url : "";

}

function get_video_record_url($bbb_record_id) {

    // sample url - https://media.appswamy.com/presentation/0ba43374d2ff36f157e8e119f169fc41fd70c917-1604588636015/video/webcams.webm

    $base_url = Setting::get('BBB_ENV') == 'production' ? Setting::get('BBB_RECORD_VIDEO_URL_PRO') : Setting::get('BBB_RECORD_VIDEO_URL_DEBUG');

    $url = $base_url.$bbb_record_id."/video/webcams.webm";

    return $url ? $url : "";

    // return remote_file_exists($url) ? $url : "";

}

function remote_file_exists($url) {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_NOBODY, true);
    curl_exec($ch);
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    if( $httpCode == 200 ){return true;}
    return false;
}


/**
 * @method multi_selected()
 *
 * @uses set selected item 
 *
 * @created Ganesh
 *
 * @updated Ganesh
 *
 * @param $array, $id, $check_key_name
 *
 * @return response of array 
 */
function multi_selected($array, $id, $check_key_name) {

    foreach ($array as $key => $value) {

        if(in_array($value->$check_key_name,$id)){

            $value->is_selected = YES;

        } else {

            $value->is_selected = NO;

        }

    }

    return $array;
}
