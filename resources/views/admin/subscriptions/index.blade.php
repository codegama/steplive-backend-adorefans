@extends('layouts.admin') 

@section('content-header', tr('subscriptions'))

@section('bread-crumb')

    <li class="breadcrumb-item"><a href="{{ route('admin.subscriptions.index')}}">{{tr('subscriptions')}}</a></li>

    <li class="breadcrumb-item active" aria-current="page">
        <span>{{ tr('view_subscriptions') }}</span>
    </li> 
           
@endsection 

@section('content')

<div class="row">

    <div class="col-12">

        <div class="card">

            <div class="card-body">

                <div class="border-bottom pb-3">

                    <h5 class="text-uppercase">{{tr('view_subscriptions')}}

                        <a class="btn btn-outline-primary float-right subscription_add" href="{{route('admin.subscriptions.create')}}">
                            <i class="fa fa-plus"></i> {{tr('add_subscription')}}
                        </a>

                        <div class="dropdown float-right">
                            <button class="btn btn-outline-primary  dropdown-toggle float-right bulk-action-dropdown text-uppercase" href="#" id="dropdownMenuOutlineButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-plus"></i> {{tr('bulk_action')}}
                            </button>

                            <div class="dropdown-menu float-right" aria-labelledby="dropdownMenuOutlineButton2">

                                <a class="dropdown-item action_list" href="#" id="bulk_delete">
                                    {{tr('delete')}}
                                </a>

                                <a class="dropdown-item action_list" href="#" id="bulk_approve">
                                    {{ tr('approve') }}
                                </a>

                                <a class="dropdown-item action_list" href="#" id="bulk_decline">
                                    {{ tr('decline') }}
                                </a>
                            </div>
                        </div>

                        <div class="bulk_action">

                            <form action="{{route('admin.subscriptions.bulk_action')}}" id="subscriptions_form" method="POST" role="search">

                                @csrf

                                <input type="hidden" name="action_name" id="action" value="">

                                <input type="hidden" name="selected_subscriptions" id="selected_ids" value="">

                                <input type="hidden" name="page_id" id="page_id" value="{{ (request()->page) ? request()->page : '1' }}">

                            </form>

                        </div>
                  
                    </h5>

                </div>

                <div class="pt-2 pb-2">

                @include('admin.subscriptions._search')


                    <table  id="basic-datatable" class="table dt-responsive nowrap">

                        <thead>
                            <tr><th>
                                      <input id="check_all" type="checkbox" class="@if(Request::get('page')) sub-chk-box-left @else chk-box-left @endif">
                                  </th>
                                <th>{{tr('s_no')}}</th>
                                <th>{{tr('title')}}</th>
                                <th>{{tr('no_of_users')}}</th>
                                <th>{{tr('no_of_minutes')}}</th>
                                <th>{{tr('subscribers')}}</th>
                                <th>{{tr('status')}}</th>
                                <th>{{tr('amount')}}</th>
                                <th>{{tr('action')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($subscriptions as $i => $subscription_details)
                                  
                                <tr>
                                   
                                    <td><input type="checkbox" name="row_check" class="faChkRnd chk-box-inner-left" id="{{$subscription_details->id}}" value="{{$subscription_details->id}}"></td>

                                    <td>{{$i+$subscriptions->firstItem()}}</td>

                                    <td>
                                        <a href="{{route('admin.subscriptions.view' , ['subscription_id' => $subscription_details->id])}}"> {{ $subscription_details->title }}
                                        </a>
                                    </td>

                                    <td>
                                        <a href="{{route('admin.subscriptions.view',['subscription_id' => $subscription_details->id])}}">
                                            {{$subscription_details->no_of_users_formatted}}
                                        </a>
                                    </td>
                                    
                                    <td>{{$subscription_details->no_of_minutes_formatted}}</td>

                                    <td><a href="{{route('admin.subscription_payments.index',['subscription_id' => $subscription_details->id])}}">{{$subscription_details->subscriptionPayments()->count()}}</a></td>

                                    <td>

                                        @if($subscription_details->status == APPROVED)

                                            <span class="badge badge-success">{{ tr('approved') }} </span>

                                        @else

                                            <span class="badge badge-danger">{{ tr('declined') }} </span>

                                        @endif

                                    </td>
                                        
                                    <td>  
                                        {{formatted_amount ($subscription_details->amount)}}           
                                    </td>

                                    <td>     

                                        <div class="template-demo">

                                            <div class="dropdown">

                                                <button class="btn btn-outline-primary  dropdown-toggle" type="button" id="dropdownMenuOutlineButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    {{tr('action')}}
                                                </button>

                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuOutlineButton1">
                                                  
                                                    <a class="dropdown-item" href="{{ route('admin.subscriptions.view', ['subscription_id' => $subscription_details->id]) }}">
                                                        {{tr('view')}}
                                                    </a>
                                                    
                                                    @if(Setting::get('is_demo_control_enabled') == NO)
                                                        <a class="dropdown-item" href="{{ route('admin.subscriptions.edit', ['subscription_id' => $subscription_details->id]) }}">
                                                            {{tr('edit')}}
                                                        </a>
                                                        
                                                        <a class="dropdown-item" href="{{route('admin.subscriptions.delete', ['subscription_id' => $subscription_details->id])}}" 
                                                        onclick="return confirm(&quot;{{tr('subscription_delete_confirmation' , $subscription_details->title)}}&quot;);">
                                                            {{tr('delete')}}
                                                        </a>
                                                    @else

                                                        <a class="dropdown-item text-muted" href="javascript:;" >{{tr('edit')}}</a>
                                                      
                                                        <a class="dropdown-item text-muted" href="javascript:;" onclick="return confirm(&quot;{{tr('subscription_delete_confirmation' , $subscription_details->title)}}&quot;);">{{tr('delete')}}</a>                           
                                                    @endif

                                                    <div class="dropdown-divider"></div>


                                                    @if($subscription_details->status == APPROVED)

                                                        <a class="dropdown-item" href="{{ route('admin.subscriptions.status', ['subscription_id' => $subscription_details->id]) }}" onclick="return confirm(&quot;{{$subscription_details->title}} - {{tr('subscription_decline_confirmation')}}&quot;);" >
                                                            {{ tr('decline') }} 
                                                        </a>

                                                    @else
                                                        
                                                        <a class="dropdown-item" href="{{ route('admin.subscriptions.status', ['subscription_id' => $subscription_details->id]) }}">
                                                            {{ tr('approve') }} 
                                                        </a>
                                                           
                                                    @endif


                                                </div>

                                            </div>

                                        </div>

                                    </td>

                                </tr>

                            @endforeach
                            
                        </tbody>
                       
                    </table>

                    <div class="float-right">{{ $subscriptions->appends(request()->input())->links() }}</div>


                </div>
            </div>
            
        </div>
        
    </div>
   
</div>


@endsection


@section('scripts')
    
@if(Session::has('bulk_action'))
<script type="text/javascript">
    $(document).ready(function(){
        localStorage.clear();
    });
</script>
@endif

<script type="text/javascript">

    $(document).ready(function(){
        get_values();

        // Call to Action for Delete || Approve || Decline
        $('.action_list').click(function(){
            var selected_action = $(this).attr('id');
            if(selected_action != undefined){
                $('#action').val(selected_action);
                if($("#selected_ids").val() != ""){
                    if(selected_action == 'bulk_delete'){
                        var message = "{{ tr('admin_subscriptions_delete_confirmation') }}";
                    }else if(selected_action == 'bulk_approve'){
                        var message = "{{ tr('admin_subscriptions_approve_confirmation') }}";
                    }else if(selected_action == 'bulk_decline'){
                        var message = "{{ tr('admin_subscriptions_decline_confirmation') }}";
                    }
                    var confirm_action = confirm(message);

                    if (confirm_action == true) {
                      $( "#subscriptions_form" ).submit();
                    }
                    // 
                }else{
                    alert('Please select the check box');
                }
            }
        });
    // single check
    var page = $('#page_id').val();
    $(':checkbox[name=row_check]').on('change', function() {
        var checked_ids = $(':checkbox[name=row_check]:checked').map(function() {
            return this.id;
        })
        .get();

        localStorage.setItem("subscription_checked_items"+page, JSON.stringify(checked_ids));

        get_values();

    });
    // select all checkbox
    $("#check_all").on("click", function () {
        if ($("input:checkbox").prop("checked")) {
            $("input:checkbox[name='row_check']").prop("checked", true);
            var checked_ids = $(':checkbox[name=row_check]:checked').map(function() {
                return this.id;
            })
            .get();
            // var page = {!! $subscriptions->lastPage() !!};
            console.log("subscription_checked_items"+page);

            localStorage.setItem("subscription_checked_items"+page, JSON.stringify(checked_ids));
            get_values();
        } else {
            $("input:checkbox[name='row_check']").prop("checked", false);
            localStorage.removeItem("subscription_checked_items"+page);
            get_values();
        }

    });

    // Get Id values for selected subscription
    function get_values(){
        var pageKeys = Object.keys(localStorage).filter(key => key.indexOf('subscription_checked_items') === 0);
        var values = Array.prototype.concat.apply([], pageKeys.map(key => JSON.parse(localStorage[key])));

        if(values){
            $('#selected_ids').val(values);
        }

        for (var i=0; i<values.length; i++) {
            $('#' + values[i] ).prop("checked", true);
        }
    }

});
</script>

@endsection