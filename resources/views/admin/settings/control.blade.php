@extends('layouts.admin') 

@section('content-header', tr('settings'))

@section('bread-crumb')

    <li class="breadcrumb-item">
        <a href="{{ route('admin.settings') }}">{{tr('settings')}}</a>
    </li>
           
@endsection 

@section('content') 
<div class="row">

    <div class="col-12">

        <div class="card">

            <div class="card-body">

                <div class="border-bottom pb-3">
                    <h5 class="text-uppercase">Control Settings</h5>
                </div>

                <form id="site_settings_save" action="{{ route('admin.settings.save') }}" method="POST" enctype="multipart/form-data" role="form">
                    @csrf

                    <div class="row">

                        <div class="form-group col-md-6">
                            <label for="name">Admin Take Count<span class="admin-required">*</span> </label>
                            <input type="number" class="form-control" id="admin_take_count" name="admin_take_count" placeholder="Admin Take Count" value="{{ old('admin_take_count') ?: Setting::get('admin_take_count')}}"/>
                        </div>

                    </div>

                    <h5>JITSI - Configuration</h5>
                    <hr>

                    <div class="row">

                        <div class="form-group col-md-6">
                            <label for="jitsi_base_url">{{tr('jitsi_base_url')}} *</label>
                            <input type="text" class="form-control" id="jitsi_base_url" name="jitsi_base_url" placeholder="Enter {{tr('jitsi_base_url')}}" value="{{Setting::get('jitsi_base_url')}}">
                        </div>

                         <div class="form-group col-md-6">

                            <label for="name">Is Jitsi?<span class="admin-required">*</span> </label>

                             <div class="form-group clearfix">

                                <div class="icheck-success d-inline">
                                    <input type="radio" id="is_jitsi" name="is_jitsi"
                                    value="1" @if(Setting::get('is_jitsi') ==  1) checked="checked" @endif>
                                    <label for="is_jitsi">On</label>
                                </div>

                                <div class="icheck-success d-inline">
                                    <input type="radio" id="is_jitsi" name="is_jitsi"
                                    value="0" @if(Setting::get('is_jitsi') ==  0) checked="checked" @endif>
                                    <label for="is_jitsi">Off</label>
                                </div>

                            </div>

                        </div>

                    </div>

                    <h5>BBB - Environment</h5>
                    <hr>

                    <div class="row">

                        <div class="form-group col-md-6">

                            <label for="name">BBB Environment<span class="admin-required">*</span> </label>

                             <div class="form-group clearfix">

                                <div class="icheck-success d-inline">
                                    <input type="radio" id="BBB_PRODUCTION" name="BBB_ENV"
                                    value="production" @if(Setting::get('BBB_ENV') ==  'production') checked="checked" @endif>
                                    <label for="BBB_PRODUCTION">Production</label>
                                </div>

                                <div class="icheck-success d-inline">
                                    <input type="radio" id="BBB_DEBUG" name="BBB_ENV"
                                    value="debug" @if(Setting::get('BBB_ENV') ==  'debug') checked="checked" @endif>
                                    <label for="BBB_DEBUG">Debug</label>
                                </div>

                            </div>

                        </div>

                        <div class="form-group col-md-6">

                            <label for="name">Recording<span class="admin-required">*</span> </label>

                             <div class="form-group clearfix">

                                <div class="icheck-success d-inline">
                                    <input type="radio" id="meeting_recording_on" name="meeting_recording"
                                    value="1" @if(Setting::get('meeting_recording') ==  '1') checked="checked" @endif>
                                    <label for="meeting_recording_on">On</label>
                                </div>

                                <div class="icheck-success d-inline">
                                    <input type="radio" id="meeting_recording_off" name="meeting_recording"
                                    value="0" @if(Setting::get('meeting_recording') ==  '0') checked="checked" @endif>
                                    <label for="meeting_recording_off">Off</label>
                                </div>

                            </div>

                        </div>

                    </div>

                    <h5>Production</h5>
                    <hr>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="name">BBB Base URL - Production<span class="admin-required">*</span> </label>
                            <input type="text" class="form-control" id="BBB_SERVER_BASE_URL_PRODUCTION" name="BBB_SERVER_BASE_URL_PRODUCTION" placeholder="{{ tr('name') }}" value="{{ old('BBB_SERVER_BASE_URL_PRODUCTION') ?: Setting::get('BBB_SERVER_BASE_URL_PRODUCTION')}}" required />
                        </div>

                        <div class="form-group col-md-6">
                            <label for="name">BBB Secret - Production<span class="admin-required">*</span> </label>
                            <input type="text" class="form-control" id="BBB_SECRET_PRODUCTION" name="BBB_SECRET_PRODUCTION" placeholder="{{ tr('name') }}" value="{{ old('BBB_SECRET_PRODUCTION') ?: Setting::get('BBB_SECRET_PRODUCTION')}}" required />
                        </div>

                        <div class="form-group col-md-6">
                            <label for="name">BBB RECORD PRESENTATION URL - PRO<span class="admin-required">*</span> </label>
                            <input type="text" class="form-control" id="BBB_RECORD_PRESENTATION_URL_PRO" name="BBB_RECORD_PRESENTATION_URL_PRO" value="{{ old('BBB_RECORD_PRESENTATION_URL_PRO') ?: Setting::get('BBB_RECORD_PRESENTATION_URL_PRO')}}" required />
                        </div>

                        <div class="form-group col-md-6">
                            <label for="name">BBB RECORD VIDEO URL - PRO<span class="admin-required">*</span> </label>
                            <input type="text" class="form-control" id="BBB_RECORD_VIDEO_URL_PRO" name="BBB_RECORD_VIDEO_URL_PRO" value="{{ old('BBB_RECORD_VIDEO_URL_PRO') ?: Setting::get('BBB_RECORD_VIDEO_URL_PRO')}}" required />
                        </div>
                    </div>

                    <h5>Staging</h5>
                    <hr>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="name">BBB Base URL - DEBUG<span class="admin-required">*</span> </label>
                            <input type="text" class="form-control" id="BBB_SERVER_BASE_URL_DEBUG" name="BBB_SERVER_BASE_URL_DEBUG" placeholder="{{ tr('name') }}" value="{{ old('BBB_SERVER_BASE_URL_DEBUG') ?: Setting::get('BBB_SERVER_BASE_URL_DEBUG')}}" required />
                        </div>

                         <div class="form-group col-md-6">
                            <label for="name">BBB Secret - DEBUG<span class="admin-required">*</span> </label>
                            <input type="text" class="form-control" id="BBB_SECRET_DEBUG" name="BBB_SECRET_DEBUG" placeholder="{{ tr('name') }}" value="{{ old('BBB_SECRET_DEBUG') ?: Setting::get('BBB_SECRET_DEBUG')}}" required />
                        </div>

                        <div class="form-group col-md-6">
                            <label for="name">BBB RECORD PRESENTATION URL - DEBUG<span class="admin-required">*</span> </label>
                            <input type="text" class="form-control" id="BBB_RECORD_PRESENTATION_URL_DEBUG" name="BBB_RECORD_PRESENTATION_URL_DEBUG" value="{{ old('BBB_RECORD_PRESENTATION_URL_DEBUG') ?: Setting::get('BBB_RECORD_PRESENTATION_URL_DEBUG')}}" required />
                        </div>

                        <div class="form-group col-md-6">
                            <label for="name">BBB RECORD VIDEO URL - DEBUG<span class="admin-required">*</span> </label>
                            <input type="text" class="form-control" id="BBB_RECORD_VIDEO_URL_DEBUG" name="BBB_RECORD_VIDEO_URL_DEBUG" value="{{ old('BBB_RECORD_VIDEO_URL_DEBUG') ?: Setting::get('BBB_RECORD_VIDEO_URL_DEBUG')}}" required />
                        </div>
                    </div>
                   
                    <button type="reset" class="btn btn-warning">{{ tr('reset')}}</button>

                    <button type="submit" class="btn btn-success mr-2">{{ tr('submit') }}</button>
                   
                </form>

            </div>

        </div>

    </div>

</div>

@endsection